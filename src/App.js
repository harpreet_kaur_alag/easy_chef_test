import Main from "./pages";
import './App.css';
// Redux setup
import { Provider } from "react-redux";
import store from "./redux/store";
function App() {
  return (
    <Provider store={store}>
      <Main/>
    </Provider>
  );
}

export default App;
