import { fade, makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    grow: {
      flexGrow: 1
    },
    formControl: {
        margin: "0 0 17px 0",
        paddingTop: "27px",
        position: "relative",
        "& svg,& .fab,& .far,& .fal,& .fas,& .material-icons": {
          color: "#495057"
        }
    },
    input: {
        color: "#495057",
        height: "unset",
        "&,&::placeholder": {
            fontSize: "14px",
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            fontWeight: "400",
            lineHeight: "1.42857",
            opacity: "1"
        },
        "&::placeholder": {
            color: "#AAAAAA"
        }
    },
    button: {
    },
    header: {
        background: "#3f51b5",
        color: "#fff",
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        fontWeight: "700",
        padding: "10px 10px",
        marginBottom: "10px"
    },
    deleteButton: {
        margin: "auto 0"
    },
    buttonReciepe: {
        maxWidth: "300px",
        float: "right"
    }
}));
export default useStyles;