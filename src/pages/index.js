import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Components
import Header from "../easyCheifComponents/Header"
import Footer from '../easyCheifComponents/Footer';
// Pages
import {
    auth,
    login
} from "./../routes"

export default function Main() {
    const getRoutes = (_routes, parent) => {
        var routes = _routes.map((_auth) => {
            return <Route 
                key={parent+_auth.path} 
                exact 
                path={_auth.path} 
                component={_auth.component}
            />
        })
        if( _routes.children &&
             Array.isArray(_routes.children)) {
            routes = [
                ...routes, 
                ...getRoutes(_routes.children, parent)
            ];
        }
        return routes;
    }
    return (
        <div className="wrapper">
            <Router>
                <Header/>
                    <Switch>
                        {getRoutes(auth, "/")} 
                        {getRoutes(login, "/")} 
                    </Switch>
                <Footer/>
            </Router>
        </div>  
    );
  }

