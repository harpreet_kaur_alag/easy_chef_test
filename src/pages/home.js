import React from "react";

// Components
import HomeComponent from "../easyCheifComponents/Home";

const Home = () => {
    return (
        <div className="flex-center">
            <HomeComponent />
        </div>
    )
}

export default Home;