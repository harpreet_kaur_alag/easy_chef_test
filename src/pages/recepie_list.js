import React from "react";

// Components
import RecepieListComponent from "../easyCheifComponents/RecepieList";

const RecepieList = () => {
    return (
        <div className="flex-center">
            <RecepieListComponent />
        </div>
    )
}

export default RecepieList;