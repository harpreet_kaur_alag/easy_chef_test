//users
export const GET_USER = "GET_USER";
export const USER_ERROR = "USER_ERROR";

//recepie
export const SET_RECEPIE = "SET_RECEPIE";
export const RECEPIE_ERROR = "RECEPIE_ERROR";

//ingrediant
export const SET_INGREDIANT = "SET_INGREDIANT";
export const INGREDIANT_ERROR = "INGREDIANT_ERROR";