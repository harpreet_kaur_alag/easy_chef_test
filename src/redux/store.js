import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import reducers from "./reducers";
const loadState = () => {
    try {
      const serializedState = localStorage.getItem('state');
      if (serializedState === null) {
        return undefined;
      }
      return JSON.parse(serializedState);
    } catch (err) {
      return undefined;
    }
}; 
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch {
        // ignore write errors
    }
};
const persistedState = loadState();
const middleware = [thunk];

const store = createStore(
    reducers,
    persistedState,
    composeWithDevTools(applyMiddleware(...middleware))
)
store.subscribe(() => {
    saveState({
        user: store.getState().user,
        recepies: store.getState().recepies,
        ingrediants: store.getState().ingrediants
    });
});
export default store;