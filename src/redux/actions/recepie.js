import { 
    SET_RECEPIE,
    RECEPIE_ERROR } from "../constants";

export const setRecepie = (data) => async (dispatch) => {
    try{
        const res = {
            data: data
        }
        dispatch({
            type: SET_RECEPIE,
            payload: res.data
        })
    }catch(err){
        dispatch({
            type: RECEPIE_ERROR,
            payload: { msg: err }
        })
    }
}