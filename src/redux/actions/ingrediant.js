import { 
    SET_INGREDIANT,
    INGREDIANT_ERROR } from "../constants";

export const setIngrediant = (data) => async (dispatch) => {
    try{
        const res = {
            data: data
        }
        dispatch({
            type: SET_INGREDIANT,
            payload: res.data
        })
    }catch(err){
        dispatch({
            type: INGREDIANT_ERROR,
            payload: { msg: err }
        })
    }
}