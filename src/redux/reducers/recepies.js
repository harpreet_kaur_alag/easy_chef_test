import { SET_RECEPIE, RECEPIE_ERROR } from "../constants";

const initialState = {
    recepies: [],
    error: null,
    recepieLoading: false
}

const recepieReducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch(type){
        case SET_RECEPIE:
            return {
                ...state,
                recepies: [...state.recepies, payload],
                recepieLoading: false
            }

        case RECEPIE_ERROR:
            return {
                ...state,
                error: payload,
                recepieLoading: false
            }
        
        default:
            return state;
    }
}

export default recepieReducer;