import { SET_INGREDIANT, INGREDIANT_ERROR } from "../constants";

const initialState = {
    ingrediants: [],
    error: null,
    ingrediantLoading: false
}

const ingrediantReducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch(type){
        case SET_INGREDIANT:
            return {
                ...state,
                ingrediants: payload,
                ingrediantLoading: false
            }

        case INGREDIANT_ERROR:
            return {
                ...state,
                error: payload,
                ingrediantLoading: false
            }
        
        default:
            return state;
    }
}

export default ingrediantReducer;