import { combineReducers } from "redux";
import user from "./user";
import recepies from "./recepies";
import ingrediants from "./ingrediants";

export default combineReducers({ 
    user,
    recepies,
    ingrediants
});