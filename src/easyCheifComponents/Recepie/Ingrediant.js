
import {
    FormControl,
    Input,
    InputLabel,
    Grid,
    IconButton
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import useStyles from "./../../style";
const Ingrediant = ({
    useField,
    index,
    handleDeleteIngrediant
}) => {
    const classes = useStyles(useStyles);
    const name = useField(`${index}_name`, `FieldArray`);
    const qty = useField(`${index}_qty`, `FieldArray`);
    const unit = useField(`${index}_unit`, `FieldArray`);
    return <Grid container spacing={2}>
        <Grid item>
            <FormControl className={classes.formControl}>
            <InputLabel htmlFor="steps">Name</InputLabel>
            <Input
                name="name"
                {...name.input}
            />
            </FormControl>
        </Grid>
        <Grid item>
            <FormControl className={classes.formControl}>
            <InputLabel htmlFor="steps">Qty</InputLabel>
            <Input
                name="qty"
                {...qty.input}
            />
            </FormControl>
        </Grid>
        <Grid item>
            <FormControl className={classes.formControl}>
            <InputLabel htmlFor="steps">Unit of measurement</InputLabel>
            <Input
                name="unit"
                {...unit.input}
            />
            </FormControl>
        </Grid>
    </Grid>
}
export default Ingrediant;