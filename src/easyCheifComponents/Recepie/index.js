import React, {useState} from "react";
import {
    FormControl,
    Input,
    InputLabel,
    Button,
    Grid,
    Typography
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import useStyles from "./../../style";
import useForm from "./../UseForm";
import {
    setRecepie
} from "../../redux/actions/recepie"
import { useDispatch } from 'react-redux'
import Ingrediant from './Ingrediant'
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux'
const config = {
    form: "login",
    initialValues: { 
        name: "",
        steps: "",
        url: ""
    }
};
const Recepie = () => {
    let history = useHistory()
    const { useField, handleSubmit } = useForm(config);
    const user = useSelector((state) => state.user.user)
    const name = useField("name");
    const steps = useField("steps");
    const url = useField("url");
    const classes = useStyles(useStyles);
    const dispatch = useDispatch();
    const [ingrediantsCount, setIngrediantsCount] = useState(1);
    const onSubmit = (formValues) => {
        dispatch(setRecepie( {...formValues, user:user} ));
        history.replace("/home");
    }
    const handleDeleteIngrediant = (index) => {
        
    }
    const handleAddMore = () => {
        setIngrediantsCount(ingrediantsCount+1);
    }
    return (
        <div className={classes.grow}>
            <form className="form"  onSubmit={handleSubmit(onSubmit)}>
                <div className={classes.header}>
                    <label>Recepie</label>
                </div>
                <Grid container spacing={2}>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="name">Name of the dish</InputLabel>
                            <Input
                                name="name"
                                {...name.input}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="steps">Steps to cook</InputLabel>
                            <Input
                                name="steps"
                                {...steps.input}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="steps">
                                Picture of dish in the form of URL
                            </InputLabel>
                            <Input
                                name="url"
                                {...url.input}
                            />
                        </FormControl>
                    </Grid>
                </Grid>
                <Typography
                     variant="h6" 
                     className={classes.title}>
                    Ingrediant
                </Typography>
                {
                    [...Array(ingrediantsCount)].map((ingrediant, index) => {
                        return <Ingrediant 
                            useField = {useField} 
                            index = {index}  
                            key={index}
                            handleDeleteIngrediant={handleDeleteIngrediant}
                        />
                    })
                }
                <Fab 
                    color="primary" 
                    aria-label="add"
                    onClick={handleAddMore}
                    className="add_icon">
                    <AddIcon />
                </Fab>
                <Button
                    type="submit" 
                    variant="contained" 
                    color="primary" 
                    className={classes.buttonReciepe}>
                    Save
                </Button>
            </form>
        </div>
    )
}

export default Recepie