import React from 'react';
//Redux
import Login from "./Login";
import Auth from "./Auth";
import { useSelector } from 'react-redux'

const Header = () => {
    const user = useSelector((state) => state.user.user)
    if( !user ) {
      return <Login />
    } else {
      return <Auth />
    }
};

export default Header;
