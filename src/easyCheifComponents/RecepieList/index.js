import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { useSelector } from 'react-redux'
import { useHistory } from "react-router-dom";
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const RecepieList = () => {
  let history = useHistory()
  const classes = useStyles();
  const recepies = useSelector((state) => state.recepies.recepies);
  const gotoRecepiForm = () => {
    history.replace("/add");
  }
  return (<div className="recepie_list">
    <Button variant="contained" color="primary" onClick={gotoRecepiForm} >
      Add New
    </Button> 
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell align="right">Steps</TableCell>
            <TableCell align="right">Picture</TableCell>
            <TableCell align="right">User</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {recepies.map((row, index) => {
            return <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.steps}</TableCell>
              <TableCell align="right">{row.url}</TableCell>
              <TableCell align="right">{row.user?row.user.username:""}</TableCell>
            </TableRow>
          })}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  );
}
export default RecepieList;