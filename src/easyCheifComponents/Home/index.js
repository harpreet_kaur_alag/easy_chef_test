import React from "react";
import {
    FormControl,
    Input,
    InputLabel,
    Button
} from '@material-ui/core';
import useStyles from "./../../style";
import useForm from "./../UseForm";
import {
    getUser
} from "../../redux/actions/user"
import { useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom";
const config = {
    form: "login",
    initialValues: { username: ""}
};
const Home = () => {
    let history = useHistory()
    const { useField, handleSubmit } = useForm(config);
    const username = useField("username");
    const classes = useStyles(useStyles);
    const dispatch = useDispatch()
    const onSubmit = (formValues) => {
        dispatch(getUser( formValues ));
        history.replace("/home");
    }
    return (
        <div className={classes.grow}>
            <form className="form login"  onSubmit={handleSubmit(onSubmit)}>
                <div className={classes.header}>
                    <label>Enter Your Name</label>
                </div>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="username">Username</InputLabel>
                    <Input
                        name="username"
                        {...username.input}
                    />
                </FormControl>
                <Button
                    type="submit" 
                    variant="contained" 
                    color="primary" 
                    className={classes.button}>
                    Next
                </Button>
            </form>
        </div>
    )
}

export default Home